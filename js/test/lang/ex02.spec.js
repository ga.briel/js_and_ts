function createUser(name, email, password, passwordConfirmation) {
  if (name.length <= 0 || name.length > 64) {
    throw new Error('validation error: invalid name size');
  }

  if (!email.match(/[a-z]+@[a-z]+\.com(\.[a-z]{2})/)) {
    throw new Error('validation error: invalid email format');
  }

  if (!password.match(/[a-z0-9]{6,20}/)) {
    throw new Error('validation error: invalid password format');
  }

  if (password !== passwordConfirmation) {
    throw new Error('validation error: confirmation does not match');
  }
}

describe(createUser, () => {
  // your code goes here
  test('incorrect name', () => {
    const user = [
      'nome'.repeat(100),
      'email@usp.com.br',
      'password',
      'password'
    ]
    const safeEnv = () => {
      const x = createUser(user[0],user[1],user[2],user[3]);
    }
    expect(safeEnv).toThrow('name');
  })

  test('incorrect email - no @', () => {
    const user = [
      'nome',
      'email!errado.com.br',
      'password',
      'password'
    ]
    const safeEnv = () => {
      const x = createUser(user[0],user[1],user[2],user[3]);
    }
    expect(safeEnv).toThrow('email');
  })

  test('incorrect email - no .com', () => {
    const user = [
      'nome',
      'email@errado.erro',
      'password',
      'password'
    ]
    const safeEnv = () => {
      const x = createUser(user[0],user[1],user[2],user[3]);
    }
    expect(safeEnv).toThrow('email');
  })

  test('incorrect email - number in domain', () => {
    const user = [
      'nome',
      'email@err4do.com.br',
      'password',
      'password'
    ]
    const safeEnv = () => {
      const x = createUser(user[0],user[1],user[2],user[3]);
    }
    expect(safeEnv).toThrow('email');
  })

  test('incorrect email - nothing after .com', () => {
    const user = [
      'nome',
      'email@errado.com',
      'password',
      'password'
    ]
    const safeEnv = () => {
      const x = createUser(user[0],user[1],user[2],user[3]);
    }
    expect(safeEnv).toThrow('email');
  })

  test('incorrect password - too short', () => {
    const user = [
      'nome',
      'email@usp.com.br',
      'short',
      'short'
    ]
    const safeEnv = () => {
      const x = createUser(user[0],user[1],user[2],user[3]);
    }
    expect(safeEnv).toThrow('password');
  })

  test('incorrect password - incorrect confirmation', () => {
    const user = [
      'nome',
      'email@usp.com.br',
      '100right',
      '100wrong'
    ]
    const safeEnv = () => {
      const x = createUser(user[0],user[1],user[2],user[3]);
    }
    expect(safeEnv).toThrow('confirmation');
  })
});
