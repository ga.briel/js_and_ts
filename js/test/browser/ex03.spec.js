const test = require('ava');

function scaffoldStructure(document, data) {
  document.documentElement.innerHTML = '';
  data.sort(function(a,b) {return(a.name.toLowerCase().localeCompare(b.name.toLowerCase()))});
  let body = document.querySelector('.body');
  let ul = document.createElement('ul');
  document.body.appendChild(ul);
  data.forEach((el, i) => {
    let li = document.createElement('li');
    let text = document.createTextNode(el.name.bold() + ' - ' + el.email);
    li.className = 'name';
    li.appendChild(text);
    ul.appendChild(li);
  });
}

test('check for correct order', t => {
  const data = [
    { name: 'Bernardo', email: 'b.ern@mail.com' },
    { name: 'Zlatan', email: 'carl@mail.com' },
    { name: 'Carla', email: 'carl@mail.com' },
    { name: 'Marcos', email: 'carl@mail.com' },
    { name: 'José', email: 'carl@mail.com' },
    { name: 'Ariel', email: 'carl@mail.com' },
    { name: 'AJ', email: 'carl@mail.com' },
    { name: 'Fabíola', email: 'fabi@mail.com' }
  ];

  scaffoldStructure(document, data);

  const nameNodes = document.querySelectorAll('.name');
  t.is(nameNodes[7].textContent, '<b>Zlatan</b> - carl@mail.com');
})

test('the creation of a page from scratch', t => {
  const data = [
    { name: 'Bernardo', email: 'b.ern@mail.com' },
    { name: 'Carla', email: 'carl@mail.com' },
    { name: 'Fabíola', email: 'fabi@mail.com' }
  ];

  scaffoldStructure(document, data);

  const nameNodes = document.querySelectorAll('.name');
  t.is(nameNodes.length, data.length);
});


test('check for unordered list', t => {
  const data = [
    { name: 'Bernardo', email: 'b.ern@mail.com' },
    { name: 'Carla', email: 'carl@mail.com' },
    { name: 'Fabíola', email: 'fabi@mail.com' }
  ];

  scaffoldStructure(document, data);

  const list = document.querySelectorAll('ul');
  t.assert(list.length > 0);
})

//mesmo teste que o primeiro, funciona para checar atributos com classe .name
test('check for .name', t => {
  const data = [
    { name: 'Bernardo', email: 'b.ern@mail.com' },
    { name: 'Carla', email: 'carl@mail.com' },
    { name: 'Fabíola', email: 'fabi@mail.com' }
  ];

  scaffoldStructure(document, data);

  const nameNodes = document.querySelectorAll('.name');
  t.assert(nameNodes.length > 0);
})

test('check for correct format', t => {
  const data = [
    { name: 'Bernardo', email: 'b.ern@mail.com' },
    { name: 'Carla', email: 'carl@mail.com' },
    { name: 'Fabíola', email: 'fabi@mail.com' }
  ];

  scaffoldStructure(document, data);

  const nameNodes = document.querySelectorAll('.name');
  t.is(nameNodes[0].textContent,'<b>Bernardo</b> - b.ern@mail.com');
})
